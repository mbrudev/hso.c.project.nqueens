/**
@file printStatus.c
@brief contains status functions
@author Mike
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn void statusLayout(struct chessboard *psChessboard)
@brief print status depending on input
@param struct chessboard *psChessboard
@return
@author Mike
@date 04.01.2018
*/

void statusLayout(struct chessboard *psChessboard)
{
   char *pcFilename = malloc((psChessboard->sSaveFile.iLength));
   static char pcMode[4];
   static char pcSave[4];
   strcpy(pcFilename, psChessboard->sSaveFile.pcFilename);

   if (psChessboard->sBitfield.bitSaveMode == save_On) {
      strcpy(pcSave, "ON ");
   }
   if (psChessboard->sBitfield.bitSaveMode == save_Off) {
      strcpy(pcSave, "OFF");
   }
   if (psChessboard->sBitfield.bitSolution == one_Solution) {
      strcpy(pcMode, "ONE");
   }
   if (psChessboard->sBitfield.bitSolution == all_Solutions) {
      strcpy(pcMode, "ALL");
   }

   // Compilerwarning (Level 4) C4204
   //struct keyVal sStatusElements[CONTROL_MENU_SIZE] = {
   //    {is_double, "Counter", "\0", psChessboard->iQueenSolutionCounter},
   //    {is_char, "Solve", pcMode, -1},
   //    {is_double, "Boardsize", "\0", psChessboard->iChessboardSize},
   //    {is_char, "Save Mode", pcSave, -1},
   //    {is_double, "Time", "\0",  psChessboard->sAppTime.dRuntime},
   //    {is_char, "Filename", pcFilename, -1}
   //};

   struct keyVal sStatusElements[CONTROL_MENU_SIZE];

   sStatusElements[0].enumsetType = is_double;
   sStatusElements[0].pcKey = "Counter";
   sStatusElements[0].pcVal = "\0";
   sStatusElements[0].dVal = psChessboard->iQueenSolutionCounter;

   sStatusElements[1].enumsetType = is_char;
   sStatusElements[1].pcKey = "Mode";
   sStatusElements[1].pcVal = pcMode;
   sStatusElements[1].dVal = -1;

   sStatusElements[2].enumsetType = is_double;
   sStatusElements[2].pcKey = "Boardsize";
   sStatusElements[2].pcVal = "\0";
   sStatusElements[2].dVal = psChessboard->iChessboardSize;

   sStatusElements[3].enumsetType = is_char;
   sStatusElements[3].pcKey = "Save Mode";
   sStatusElements[3].pcVal = pcSave;
   sStatusElements[3].dVal = -1;

   sStatusElements[4].enumsetType = is_double;
   sStatusElements[4].pcKey = "Time";
   sStatusElements[4].pcVal = "\0";
   sStatusElements[4].dVal = psChessboard->sAppTime.dRuntime;

   sStatusElements[5].enumsetType = is_char;
   sStatusElements[5].pcKey = "Filename";
   sStatusElements[5].pcVal = pcFilename;
   sStatusElements[5].dVal = -1;

   printSymbol(0, 17, '-');
   posElements(13, 9, sStatusElements);
   printSymbol(0, 20, '-');
}