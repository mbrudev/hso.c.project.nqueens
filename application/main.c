/**
@file: main.c
@brief: Contains main program
@author: Mike
@date: 04.01.2018
*/

#include "../common_includes/nqeens.h"

void menuLayout();
void statusLayout(struct chessboard *);
void controls(struct chessboard *);
void initProgram(struct chessboard *);

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS
@author Mike
@date 04.01.2018
*/
int main(void)
{
   struct chessboard structchessboard;

   // defines an rec with the size from the window
   SMALL_RECT sWindowSize = { 0 , 0 , CONSOLE_WIDTH, CONSOLE_HEIGHT};
   // sets the size from the window
   SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), TRUE, &sWindowSize);

   initProgram(&structchessboard);
   menuLayout();
   statusLayout(&structchessboard);
   controls(&structchessboard);
   return EXIT_SUCCESS;
}

