/**
@file chessboard.c
@brief Contains set chessboard size
@author Mike 
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn void setBoardSize(struct chessboard *psChessboard)
@brief set chessboard size
@param struct chessboard *psChessboard
@return
@author Mike
@date 04.01.2018
*/
void setBoardSize(struct chessboard *psChessboard)
{
   char *ac = malloc(sizeof(char *));
   do {
	   printf("Enter Size between %i and %i\n", MIN_BOARD_SIZE, MAX_BOARD_SIZE);
      scanf("%s", ac);
	  psChessboard->iChessboardSize = (short) atoi(ac);
   }
   while ((psChessboard->iChessboardSize < MIN_BOARD_SIZE || psChessboard->iChessboardSize > MAX_BOARD_SIZE));
}