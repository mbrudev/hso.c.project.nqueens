/**
@file printConsole.c
@brief prints board with solutions
@author Mike 
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn int printChessBoard(struct chessboard *pChessboard)
@brief print solved nqueens chessboard to console
@param struct chessboard *pChessboard
@return EXIT_SUCCESS
@author Mike
@date 04.01.2018
*/

int printChessBoard(struct chessboard *pChessboard)
{
   for (int i = 0; i < pChessboard->iChessboardSize; i++) {
      _gotoxy((short) ((8 * GUI_X_POS) - pChessboard->iChessboardSize), (short) (1 + (i + MIN_BOARD_SIZE)));
      for (short j = 0; j < pChessboard->iChessboardSize; j++) {
         if (pChessboard->aiChessMatrix[i] == j)
            printf("1 ");
         else
            printf("0 ");
      }
      printf("\n");
   }
   _gotoxy(0, 21);
   return EXIT_SUCCESS;
}