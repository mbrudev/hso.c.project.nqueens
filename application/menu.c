/**
@file menu.c
@brief contains menu elements
@author Mike 
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn void menuLayout(void)
@brief init menu elements and set up for print
@param void
@return
@author Mike
@date 04.01.2018
*/
void menuLayout(void)
{
   _gotoxy(33, 0);
   printf(APP_NAME"\n");
   printSymbol(0, 1, '-');

   struct keyVal sMenuElements[CONTROL_MENU_SIZE] = {
       {is_char, "- b - Set Board Size", "\0", -1},
       {is_char, "- u - Save File", "\0", -1},
       {is_char, "- r - ONE Solution", "\0", -1},
       {is_char, "- s - Stop Program", "\0", -1},
       {is_char, "- c - ALL Solutions", "\0", -1},
       {is_char, "- e - Exit Program", "\0", -1}
   };

   posElements(13, 1, sMenuElements);
   printSymbol(0, 4, '-');
}