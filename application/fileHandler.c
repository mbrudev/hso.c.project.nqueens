/**
@file fileHandler.c
@brief
@author Mike
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn void setFile(struct chessboard *psChessboard)
@brief set file to save solutions
@param struct chessboard *psChessboard
@return
@author Mike
@date 04.01.2018
*/
void setFile(struct chessboard *psChessboard)
{
   psChessboard->sSaveFile.pcFilename = (char *) malloc((sizeof(struct chessboard)));
   psChessboard->sSaveFile.pcExtension = (char *) malloc((sizeof(char *)));
   printf("Enter a Filename\n");
   scanf("%s", psChessboard->sSaveFile.pcFilename);
   strcat(psChessboard->sSaveFile.pcFilename, strcpy(psChessboard->sSaveFile.pcExtension, ".txt"));
   psChessboard->sSaveFile.iLength = strlen(psChessboard->sSaveFile.pcFilename);
}
