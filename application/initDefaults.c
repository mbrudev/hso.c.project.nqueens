/**
@file: initDefaults.c
@brief: Initialize program
@author: Mike
@date: 06.01.2018
*/

#include "../common_includes/nqeens.h"

/**
@fn void initProgram(struct chessboard *psChessboard)
@brief init Program or restores default
@param struct chessboard *psChessboard
@return
@author Mike
@date 06.01.2018
*/
void initProgram(struct chessboard *psChessboard) {
	psChessboard->iQueenSolutionCounter = 0;
	psChessboard->iChessboardSize = MAX_BOARD_SIZE - MIN_BOARD_SIZE;

	psChessboard->sBitfield.bitSolution = one_Solution;
	psChessboard->sBitfield.bitSaveMode = save_Off;
	psChessboard->sBitfield.bitCancel = 0;
	psChessboard->sBitfield.bitExit = 0;

	psChessboard->sSaveFile.pcFilename = "UNNAMED";
	psChessboard->sSaveFile.iLength = 0;

	psChessboard->sAppTime.dRuntime = 0;
	psChessboard->sAppTime.cStart_t = 0;
	psChessboard->sAppTime.cEnd_t = 0;
}
