/**
@file: positioningGui.c
@brief: contains output logic for menuLayout and statusLayout
@author: Mike
@date: 04.01.2018
*/

#include "../common_includes/nqeens.h"

/**
@fn void posElements(const short iPosX, const short iPosY, struct key_val *psKeyVal)
@brief prints struct key_val
@param const short iPosX, const short iPosY, struct key_val *psKeyVal
@return
@author Mike
@date 04.01.2018
*/
void posElements(const short iPosX, const short iPosY, struct keyVal *psKeyVal)
{
   for (int i = 0; i < CONTROL_MENU_SIZE; i++) {
      if ((i + 2) % 2 == 0) {
         _gotoxy((short) (GUI_X_POS + (i * iPosX)), (short) (GUI_Y_POS * iPosY));

         if (psKeyVal[i].enumsetType == is_char) {
            printf("%s %s", psKeyVal[i].pcKey, psKeyVal[i].pcVal);
         }
         else {
            printf("%s%s %.2lf", psKeyVal[i].pcKey, psKeyVal[i].pcVal, psKeyVal[i].dVal);
         }
      }
      else {
         _gotoxy((short) (GUI_X_POS + (i * (iPosX)) - (iPosX)), (short) (1 + GUI_Y_POS * iPosY));
         if (psKeyVal[i].enumsetType == is_char) {
            printf("%s %s", psKeyVal[i].pcKey, psKeyVal[i].pcVal);
         }
         else {
            printf("%s%s %.2lf", psKeyVal[i].pcKey, psKeyVal[i].pcVal, psKeyVal[i].dVal);
         }
      }
   }
}