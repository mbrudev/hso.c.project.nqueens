/**
@file terminate.c
@brief sets conditions to terminate
@author Mike
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn void checkTerminate(const char ccInput, struct chessboard *psChessboard)
@brief check for terminate values
@param const char ccInput, struct chessboard *psChessboard
@return
@author Mike
@date 04.01.2018
*/
void checkTerminate(const char ccInput, struct chessboard *psChessboard) {
	const char ccStop = 's';
	const char ccExit = 'e';
	if (ccInput == ccStop)
	{
		psChessboard->sBitfield.bitCancel = TERMINATE;
		printf("Algorithm has stopped! To initialize Program press - %c - again\n", ccStop);
	}
	if (ccInput == ccExit)
	{
		psChessboard->sBitfield.bitExit = TERMINATE;
	}
}