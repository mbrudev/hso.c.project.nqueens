/**
@file controls.c
@brief contains control function
@author Mike
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn void controls(struct chessboard *psChessboard)
@brief control menu
@param struct chessboard *psChessboard
@return
@author Mike
@date 04.01.2018
*/
void controls(struct chessboard *psChessboard)
{
   char cInput = '0';
   while (psChessboard->sBitfield.bitExit != TERMINATE) {
      cInput = (char) getchar();
      switch (tolower(cInput)) {
	  case 's': initProgram(psChessboard);
		  clearScreen(18, 20);
		  clearScreen(1 + MIN_BOARD_SIZE, 1 + MIN_BOARD_SIZE + MAX_BOARD_SIZE);
		  statusLayout(psChessboard);
		  break;
         case 'u':psChessboard->sBitfield.bitSaveMode = (unsigned int) !psChessboard->sBitfield.bitSaveMode;
            if (psChessboard->sBitfield.bitSaveMode == save_On) {
               setFile(psChessboard);
            }
            statusLayout(psChessboard);
            break;
         case 'b':setBoardSize(psChessboard);
            statusLayout(psChessboard);
            break;
         case 'r':statusLayout(psChessboard);
			 psChessboard->sBitfield.bitSolution = one_Solution;
			 psChessboard->sAppTime.cStart_t = clock();
			solveQueens(0, psChessboard);
            statusLayout(psChessboard);
            break;
         case 'c':statusLayout(psChessboard);
			 psChessboard->sBitfield.bitSolution = all_Solutions;
			 psChessboard->sAppTime.cStart_t = clock();
			 solveQueens(0, psChessboard);
            statusLayout(psChessboard);
            break;
		 case 'e': printf("Exit Program\n");
			 psChessboard->sBitfield.bitExit = TERMINATE;
			 break;
         default:printf("Press any key to continue!\n");
            break;
	  }
      getchar();
	  clearScreen(21, 31);
	  _gotoxy(0, 21);
   }
}

