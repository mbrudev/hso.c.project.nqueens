/**
@file: consolePrintHelpers.c
@brief: Contains helper functions for console window
@author: Mike
@date: 06.01.2018
*/

#include "../common_includes/nqeens.h"

/**
@fn void printSymbol(short iPosX, short iPosY, const char ccSymbol)
@brief print specific symbols on x and y coordinates
@param short iPosX, short iPosY, const char ccSymbol
@return
@author Mike
@date 06.01.2018
*/
void printSymbol(const short iPosX, const short iPosY, const char ccSymbol)
{
	_gotoxy(iPosX, iPosY);
	for (int i = 0; i < CONSOLE_WIDTH; i++) {
		printf("%c", ccSymbol);
	}
	printf("\n");
}

void clearScreen(const short iPosY0, const short iPosY1) {
	_gotoxy(0 , iPosY0);
	for (int i = iPosY0; i < iPosY1; i++)
	{
		printSymbol((short) 0, (short) i, ' ');
	}
}