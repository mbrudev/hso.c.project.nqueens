/**
@file solve_nqueens.c
@brief contains algorithm to solve nqueen problem
@author Mike 
@date 28.12.2017
*/

#include "../common_includes/nqeens.h"

/**
@fn int setQueen(int row, int col, struct chessboard *psChessboard)
@brief set queen on board
@param int iRow, int iCol, struct chessboard *psChessboard
@return int iRetVal
@author Mike
@date 04.01.2018
*/
int setQueen(short iRow, short iCol, struct chessboard *psChessboard)
{
   int iRetVal = EXIT_FAILURE;
   for (short i = 0; i < iRow; i++) {
      if (psChessboard->aiChessMatrix[i] == iCol) {
         iRetVal = EXIT_SUCCESS;
      }
      else {
         if (abs(psChessboard->aiChessMatrix[i] - iCol) == abs(i - iRow)) // abs = absolute. => amount of the number
         {
            iRetVal = EXIT_SUCCESS;
         }
      }
   }
   return iRetVal;
}

/**
@fn int solveQueens(int iRow, int iSize, struct chessboard *psChessboard)
@brief Algorithm to solve nqueens
@param int iRow, int iSize, struct chessboard *psChessboard
@return int iRetValSolveQueens
@author Mike
@date 04.01.2018
*/
int solveQueens(short iRow, struct chessboard *psChessboard)
{
	char cInput = '0';
   int iRetValSolveQueens = -1;
	   for (short i = 0; i < psChessboard->iChessboardSize; i++) {		  
		   if (setQueen(iRow, i, psChessboard)) {
			   psChessboard->aiChessMatrix[iRow] = i;
			   if (iRow == (psChessboard->iChessboardSize - 1)) {
				   psChessboard->iQueenSolutionCounter++;
				   statusLayout(psChessboard);
				   if (psChessboard->sBitfield.bitSolution == one_Solution) {
					   printChessBoard(psChessboard);
					   cInput = (char) _getch();
					   checkTerminate(cInput, psChessboard);
				   }
				   if (psChessboard->sBitfield.bitSolution == all_Solutions)
					{
					   if (psChessboard->sBitfield.bitSaveMode == save_On)
					   {
						   writeChessBoard(psChessboard->sSaveFile.pcFilename, psChessboard->iQueenSolutionCounter, psChessboard->iChessboardSize, psChessboard->aiChessMatrix);
					   }
					   if (_kbhit())
					   {
						   cInput = (char)_getch();
						   checkTerminate(cInput, psChessboard);		
					   }
				   }
			   }
			   else if (!psChessboard->sBitfield.bitExit && !psChessboard->sBitfield.bitCancel)
			   {
				   iRetValSolveQueens = solveQueens((iRow + 1), psChessboard);
			   }
		   }		 
	   }
	   psChessboard->sAppTime.cEnd_t = clock(); 
	   psChessboard->sAppTime.dRuntime = (double) (psChessboard->sAppTime.cEnd_t - psChessboard->sAppTime.cStart_t) / CLOCKS_PER_SEC;
   return iRetValSolveQueens;
}