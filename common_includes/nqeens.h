/**
 @file nqueens.h
 @brief Declarations for the nQueens problem
 @author Mike
 @date 04.01.2018
 */

#pragma warning(disable:4996)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <conio.h>
#include <time.h>
#include <windows.h>
#include "utilities.h"
#include "../dll/nqueens_dll.h"

 /**
 @def TERMINATE
 @brief Set termination
 */
#define TERMINATE 1

/**
@def APP_NAME
@brief Application Name
 */
#define APP_NAME "N Q U E E N S"

/**
@def MAX_QUEENS
@brief Max Queens which can set on the board
 */
#define MAX_QUEENS 12

/**
@def MIN_BOARD_SIZE
@brief min chessboard size
 */
#define MIN_BOARD_SIZE 4

/**
@def MAX_BOARD_SIZE
@brief max chessboard size
 */
#define MAX_BOARD_SIZE 12

/**
@def CONTROL_MENU_SIZE
@brief size of elements for menu and status
 */
#define CONTROL_MENU_SIZE 6

/**
@def GUI_X_POS
@brief positioning something on x coordinate
 */
#define GUI_X_POS 5

/**
@def GUI_Y_POS
@brief positioning something on y coordinate
 */
#define GUI_Y_POS 2

/**
@def CONSOLE_WIDTH
@brief max width of win 10 console window
 */
#define CONSOLE_WIDTH 80

 /**
 @def CONSOLE_WIDTH
 @brief max width of win 10 console window
 */
#define CONSOLE_HEIGHT 24

/**
@enum saveMode
@brief set save mode
 */
enum saveMode
{
   save_Off = 0,
   save_On = 1
};

/**
@enum solutionMode
@brief set solution mode (continuous or step by step)
 */
enum solutionMode
{
   one_Solution = 0,
   all_Solutions = 1
};

/**
@enum set_type
@brief enum to to specify data type
 */
enum setDataType
{
	is_double, 
	is_char
};

/**
@struct bitfield
@brief set bit for specific mode
 */
struct bitfield
{
   unsigned int bitSolution: 1;
   unsigned int bitSaveMode: 1;
   unsigned int bitCancel: 1;
   unsigned int bitExit : 1;
};

/**
@struct saveFile
@brief data type to set file depending values
 */
struct saveFile
{
   size_t iLength;
   char *pcFilename;
   char *pcExtension;
};

/**
@struct appTime
@brief measuring runtime
 */
struct appTime
{
   clock_t cStart_t;
   clock_t cEnd_t;
   double dRuntime;
};

/**
@struct chessboard
@brief data type for solving the nqueens problem
 */
struct chessboard
{
   short aiChessMatrix[MAX_QUEENS];
   short iChessboardSize;
   size_t iQueenSolutionCounter;
   struct appTime sAppTime;
   struct saveFile sSaveFile;
   struct bitfield sBitfield;
};

/**
@struct keyVal
@brief data type to set key_val values for menuLayout and statusLayout
 */
struct keyVal
{
   enum setDataType enumsetType;
   char *pcKey;
   char *pcVal;
   double dVal;
};

int printChessBoard(struct chessboard *);
int setQueen(short, short, struct chessboard *);
int solveQueens(short, struct chessboard *);
void checkTerminate(const char ccInput, struct chessboard *psChessboard);
void clearScreen(const short iPosY0, const short iPosY1);
void controls(struct chessboard *);
void initProgram(struct chessboard *);
void menuLayout();
void posElements(const short iPosX, const short iPosY, struct keyVal *psKeyVal);
void printSymbol(const short iPosX, const short iPosY, const char ccSymbol);
void setBoardSize(struct chessboard *);
void setFile(struct chessboard *);
void statusLayout(struct chessboard *);





