#ifdef DLL_EXPORTS
#define DLL_API __declspec(dllimport) 
#else
#define DLL_API __declspec(dllexport)  
#endif

DLL_API int writeChessBoard(const char*, size_t, const short psSize, const short psMatrix[]);