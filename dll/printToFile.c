/**
@file: printToFile.c
@brief: Dll: print into file
@author: Mike
@date: 04.01.2018
*/

#include "../common_includes/nqeens.h"

/**
@fn int writeFileChessBoard(const char * psFile, int psSolutions, int psSize, int psMatrix[])
@brief prints Solutions into File
@param const char * psFile, int psSolutions, int psSize, int psMatrix[]
@return EXIT_SUCCESS
@author Mike
@date 04.01.2018
*/

int writeChessBoard(const char* psFile, size_t psSolutions, const short psSize, const short psMatrix[])
{
   FILE *pf;
   pf = fopen(psFile, "a+");

   fprintf(pf, "Solution(s): %i\n", psSolutions);
   for (int i = 0; i < psSize; i++) {
      for (int j = 0; j < psSize; j++) {
         if (psMatrix[i] == j)
            fprintf(pf, "1 ");
         else
            fprintf(pf, "0 ");
      }
      fprintf(pf, "\n");
   }
   fclose(pf);
   return EXIT_SUCCESS;
}